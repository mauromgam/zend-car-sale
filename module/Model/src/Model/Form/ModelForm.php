<?php
/**
 * ModelForm
 *
 * @author Mauro Marinho
 */
namespace Model\Form;

use Zend\Form\Form;
use Zend\Db\TableGateway\TableGateway;

class ModelForm extends Form
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway, $name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);

        $this->tableGateway = $tableGateway;

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'maker_id',
            'type' => 'Select',
            'options' => array(
                'label' => 'Maker',
                'empty_option' => 'Please select a maker',
                'value_options' => $this->getOptionsForMakerSelect(),
            )
        ));

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Model',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }

    public function getOptionsForMakerSelect()
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $sql       = 'SELECT id, name FROM vehicle_maker ORDER BY name ASC';
        $statement = $dbAdapter->query($sql);
        $result    = $statement->execute();

        $selectData = array();

        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }
}