<?php
namespace Model\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Paginator;

class ModelTable
{
    protected $table;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->table = 'vehicle_model';
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if ($paginated) {
            // create a new Select object for the table model
            $select = new Select($this->table);
            $select->join(array('vm' => 'vehicle_maker'),
                "{$this->table}.maker_id = vm.id",
                array('maker_id' => 'name'));
            $select->columns(array(
                'id',
                'name',
            ));
            // create a new result set based on the Model entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getModelsByMaker($maker_id)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select('vehicle_model');
        $select->where(array('maker_id' => $maker_id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getModel($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function saveModel(Model $model)
    {
        $data = array(
            'name'     => $model->name,
            'maker_id' => $model->maker_id,
        );

        $id = (int) $model->id;
        if ($id == 0) {
            try {
               $this->tableGateway->insert($data);
            } catch (\Exception $e) {
                var_dump($e);
            }
        } else {
            if ($this->getModel($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Model id does not exist');
            }
        }
    }

    public function deleteModel($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}