<?php
/**
 * ModelController
 *
 * @author Mauro Marinho
 */
namespace Model\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model\Model\Model;
use Model\Form\ModelForm;
use Zend\Json\Json;

class ModelController extends AbstractActionController
{
    private $modelTable;

    /**
    *
    * Action that retrieves all models
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function indexAction()
    {
        // grab the paginator from the ModelTable
        $paginator = $this->getModelTable()->fetchAll(true);

        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page
        $paginator->setItemCountPerPage(15);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }

    /**
    *
    * Action that retrieves all models
    * based on a maker - Ajax call
    *
    * @return ViewModel
    *
    */
    public function ajaxModelsAction()
    {
        $request = $this->getRequest();
        $maker_id = $request->getPost('maker_id');
        $models = $this->getModelTable()->getModelsByMaker($maker_id);

        $data = array();
        foreach ($models as $model) {
            $data[] = [
                'id'   => $model['id'],
                'name' => $model['name']
            ];
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('');
        $viewModel->setTerminal($request->isXmlHttpRequest());

        return $viewModel->setVariables(array(
            'ajax' => Json::encode($data, true)
        ));
    }

    /**
    *
    * Action that renders the add view
    * to create new models
    *
    * @return array
    *
    */
    public function addAction()
    {
        $sm = $this->getServiceLocator();
        $form = new ModelForm($sm->get('MakerTableGateway'), 'add_model_form');
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $model = new Model();
            $form->setInputFilter($model->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $model->exchangeArray($form->getData());
                $this->getModelTable()->saveModel($model);

                // Redirect to list of models
                return $this->redirect()->toRoute('zfcadmin/models');
            }
        }
        return array('form' => $form);
    }

    /**
    *
    * Action that renders the edit view
    * to edit models
    *
    * @return array
    *
    */
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/models', array(
                'action' => 'add'
            ));
        }

        // Get the Model with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $model = $this->getModelTable()->getModel($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('zfcadmin/models', array(
                'action' => 'index'
            ));
        }

        $sm = $this->getServiceLocator();
        $form = new ModelForm($sm->get('MakerTableGateway'), 'edit_model_form');
        $form->bind($model);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($model->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getModelTable()->saveModel($model);

                // Redirect to list of models
                return $this->redirect()->toRoute('zfcadmin/models');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
    *
    * Action that renders the delete view
    * to remove models
    *
    * @return array
    *
    */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/models');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getModelTable()->deleteModel($id);
            }

            // Redirect to list of models
            return $this->redirect()->toRoute('zfcadmin/models');
        }

        return array(
            'id'    => $id,
            'model' => $this->getModelTable()->getModel($id)
        );
    }

    /**
    *
    * Method to get the modelTable object
    * if it's not set yet, the service locator
    * is called and retrieves an instance of it
    *
    * @return ModelTable
    *
    */
    public function getModelTable()
    {
        if (!$this->modelTable) {
            $sm = $this->getServiceLocator();
            $this->modelTable = $sm->get('Model\Model\ModelTable');
        }
        return $this->modelTable;
    }
}
