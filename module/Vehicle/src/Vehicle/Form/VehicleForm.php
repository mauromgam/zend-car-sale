<?php
/**
 * VehicleForm
 *
 * @author Mauro Marinho
 */
namespace Vehicle\Form;

use Zend\Form\Form;
use Zend\Db\TableGateway\TableGateway;

class VehicleForm extends Form
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway, $name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);

        $this->tableGateway = $tableGateway;

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'maker_id',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'maker-select',
            ),
            'options' => array(
                'label' => 'Maker',
                'value_options' => $this->getOptionsForMakerSelect(),
            )
        ));

        $model_id = new \Zend\Form\Element\Select('model_id');
        $model_id->setAttributes(array(
                'id' => 'model_id',
                'class' => 'model-select',
            ));
        $model_id->setDisableInArrayValidator(true);
        $options = array(
            'label' => 'Model',
            'value_options' => $this->getOptionsForModelSelect(),
        );
        $model_id->setValueOptions($options);
        $this->add($model_id);

        $this->add(array(
            'name' => 'year',
            'type' => 'Text',
            'attributes' => array(
                'maxlength' => '4',
            ),
            'options' => array(
                'label' => 'Year',
            ),
        ));

        $this->add(array(
            'name' => 'registration',
            'type' => 'Text',
            'attributes' => array(
                'maxlength' => '20',
            ),
            'options' => array(
                'label' => 'Registration',
            ),
        ));

        $this->add(array(
            'name' => 'engine_size',
            'type' => 'Text',
            'attributes' => array(
                'maxlength' => '10',
            ),
            'options' => array(
                'label' => 'Engine Size',
            ),
        ));

        $this->add(array(
            'name' => 'price',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'money',
            ),
            'options' => array(
                'label' => 'Price',
            ),
        ));

        $this->add(array(
            'name' => 'keyword',
            'type' => 'Textarea',
            'attributes' => array(
                'rows' => '5',
                'cols' => '50',
            ),
            'options' => array(
                'label' => 'Keywords',
            ),
        ));

        $this->add(array(
            'name' => 'active',
            'type' => 'Select',
            'options' => array(
                'label' => 'Activate',
                'value_options' => array(
                    'Active' => 'Active',
                    'Inactive' => 'Inactive',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }

    public function getOptionsForMakerSelect()
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $sql       = 'SELECT id, name FROM vehicle_maker ORDER BY name ASC';
        $statement = $dbAdapter->query($sql);
        $result    = $statement->execute();

        $selectData = array();

        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }

    public function getOptionsForModelSelect()
    {

        $dbAdapter = $this->tableGateway->getAdapter();
        $sql       = "
            SELECT
                m.id,
                m.name
            FROM
                vehicle_model m
            WHERE
                m.maker_id = (SELECT mak.id FROM vehicle_maker mak ORDER BY mak.id LIMIT 1)
            ORDER BY m.name ASC;";
        $statement = $dbAdapter->query($sql);
        $result    = $statement->execute();

        $selectData = array();

        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }
}