<?php
namespace Vehicle\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Paginator;
use Elasticsearch\Client;
use Application\Model\Utils;

class VehicleTable
{
    protected $table;
    protected $tableGateway;
    protected $elasticSearchClient;

    public function __construct(TableGateway $tableGateway, Client $elasticSearchClient)
    {
        $this->table = 'vehicle';
        $this->tableGateway = $tableGateway;
        $this->elasticSearchClient = $elasticSearchClient;
    }

    public function fetchAll($paginated=false, $id=0)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        if ($paginated) {

            $select = $this->getVehicleSQLSelect($sql, $id);

            // create a new result set based on the Vehicle entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Vehicle());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;

        } else {

            $select = $this->getVehicleSQLSelect($sql, $id);

            $selectString = $sql->getSqlStringForSqlObject($select);
            $results = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

            return $results;
        }
    }

    public function getVehicle($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function getVehicleSQLSelect($sql, $id)
    {
        // create a new Select object for the table vehicle
        $select = $sql->select($this->table);
        $select->join("vehicle_maker",
                "{$this->table}.maker_id = vehicle_maker.id",
                array("maker_id" => "name"))
            ->join("vehicle_model",
                "{$this->table}.model_id = vehicle_model.id",
                array("model_id" => "name"));
        $select->columns(array(
            'id',
            'year',
            'registration',
            'engine_size',
            'price',
            'keyword',
            'active',
        ));
        if ($id != 0) {
            $where = new Where();
            $where->equalTo("{$this->table}.id", $id);
            $select->where($where);
        }
        $select->order(array(
            "vehicle_maker.name ASC",
            "vehicle_model.name ASC",
            "{$this->table}.engine_size ASC",
        ));

        return $select;
    }

    public function getVehicleModelId($id)
    {
        $id  = (int) $id;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select($this->table);
        $select->columns(array(
            'model_id',
        ));
        $where = new Where();
        if ($id != 0) {
            $where->equalTo("{$this->table}.id", $id);
        }
        $select->where($where);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

        if (!$results) {
            throw new \Exception("Could not find row $id");
        }

        $data = array();
        foreach ($results as $model) {
            $data[] = [
                'id'   => $model['model_id']
            ];
        }

        return $data;
    }

    public function saveVehicle(Vehicle $vehicle)
    {
        $vehicle->price = str_replace(Utils::DECIMAL_SEPARATOR, '', $vehicle->price);
        $vehicle->price = str_replace(Utils::THOUSAND_SEPARATOR, Utils::DECIMAL_SEPARATOR, $vehicle->price);
        $vehicle->price = number_format($vehicle->price, 2, Utils::DECIMAL_SEPARATOR, '');

        $data = array(
            'maker_id'     => $vehicle->maker_id,
            'model_id'     => $vehicle->model_id,
            'year'         => $vehicle->year,
            'registration' => $vehicle->registration,
            'engine_size'  => $vehicle->engine_size,
            'price'        => $vehicle->price,
            'keyword'      => $vehicle->keyword,
            'active'       => $vehicle->active,
        );

        $id = (int) $vehicle->id;
        $return = false;

        VehicleElasticSearch::checkIndexExist();

        // remove from ElasticSearch index if vehicle is disabled
        if ($id != 0 && $data['active'] != 'Active') {
            if (VehicleElasticSearch::checkIndexExist($id)->getStatusCode() == 200) {
                $params = array();
                $params['index'] = 'vehicles';
                $params['type'] = 'vehicle';
                $params['id'] = $id;
                $this->elasticSearchClient->delete($params);
            }
        }

        if ($id == 0) {
            try {
                $return = $this->tableGateway->insert($data);
            } catch (\Exception $e) {
                var_dump($e);
            }
        } else {
            if ($this->getVehicle($id)) {
                $return = $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Vehicle id does not exist');
            }
        }

        if ($return === 1) {
            /* Create ElasticSearch index */
            if (!$id) {
                $id = $this->tableGateway->lastInsertValue;
            }
            $last = $this->fetchAll(false, $id);
            foreach ($last as $l) {
                $vehicle = $l;
            }

            $keywords = explode(',', $vehicle->keyword);
            foreach ($keywords as $key => $value) {
                $keywords[$key] = trim($value);
            }

            $params = array();
            $params['body']  = array(
                'id'           => $id,
                'maker_id'     => $vehicle->maker_id,
                'model_id'     => $vehicle->model_id,
                'year'         => $vehicle->year,
                'registration' => $vehicle->registration,
                'engine_size'  => $vehicle->engine_size,
                'price'        => $vehicle->price,
                'keyword'      => $keywords,
                'active'       => $vehicle->active,
            );
            $params['index'] = 'vehicles';
            $params['type']  = 'vehicle';
            $params['id']    = $id;
            if ($data['active'] == 'Active') {
                $this->elasticSearchClient->index($params);
            }
        }
    }

    public function deleteVehicle($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
        VehicleElasticSearch::checkIndexExist();
        $params = array();
        $params['index'] = 'vehicles';
        $params['type'] = 'vehicle';
        $params['id'] = $id;
        $this->elasticSearchClient->delete($params);
    }
}