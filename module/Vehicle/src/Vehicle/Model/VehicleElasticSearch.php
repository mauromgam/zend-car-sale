<?php
namespace Vehicle\Model;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;


/**
 * Description of VehicleElasticSearch
 *
 * @author mauro_000
 */
class VehicleElasticSearch {

    const BASE_ELASTIC_SEARCH_URL = "http://127.0.0.1:9200/vehicles";
    const ACTIVE_STATUS = "Active";

    public static function getElasticSearchResults($q)
    {
        $client = new Client(self::BASE_ELASTIC_SEARCH_URL."/vehicle/_search?$q&size=8");
        $client->setMethod('GET');
        $client->setHeaders(array(
            'Content-Type: application/json',
        ));
        $client->setAdapter(new Curl());
        $return = $client->send();
        $content = Json::decode($return->getContent());

        $vehicles = array();
        if (isset($content->hits->hits)) {
            foreach ($content->hits->hits as $vehicle) {
                if ($vehicle->_source->active == self::ACTIVE_STATUS) {
                    $vehicles[] = [
                        'id' => $vehicle->_source->id,
                        'maker_id' => $vehicle->_source->maker_id,
                        'model_id' => $vehicle->_source->model_id,
                        'year' => $vehicle->_source->year,
                        'registration' => $vehicle->_source->registration,
                        'engine_size' => $vehicle->_source->engine_size,
                        'price' => $vehicle->_source->price,
                        'keyword' => $vehicle->_source->keyword,
                        'active' => $vehicle->_source->active
                    ];
                }
            }
        }
        return $vehicles;
    }

    public static function checkIndexExist($vehicle=0)
    {
        $search_index = "";
        if ($vehicle != 0) {
            $search_index = "/vehicle/$vehicle";
        }
        $client = new Client(self::BASE_ELASTIC_SEARCH_URL.$search_index);
        $client->setMethod('HEAD');
        $client->setHeaders(array(
            'Content-Type: application/json',
        ));
        $curl = new Curl();
        $client->setAdapter($curl);
        $return = $client->send();
        if ($return->getStatusCode() != 200 && $search_index == 0) {
            // If index doesn't exist, create it with some configs
            $body = '{"index":{"name":"vehicles","number_of_shards":1,"analysis":{"analyzer":{"translation_index_analyzer":{"type":"custom","tokenizer":"standard","filter":"standard,lowercase,translation_tokenizer"},"translation_search_analyzer":{"type":"custom","tokenizer":"standard","filter":"standard,lowercase"}},"filter":{"translation_tokenizer":{"type":"nGram","min_gram":"1","max_gram":"10"}}}}}';
            $client = new Client(self::BASE_ELASTIC_SEARCH_URL);
            $client->setMethod('PUT');
            $client->setRawBody($body);
            $client->setHeaders(array(
                'Content-Type: application/json',
            ));
            $client->setAdapter($curl);
            $return = $client->send();
        }
        return $return;
    }
}
