<?php
/**
 * VehicleController
 *
 * @author Mauro Marinho
 */
namespace Vehicle\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Vehicle\Model\Vehicle;
use Vehicle\Form\VehicleForm;
use Zend\Json\Json;

class VehicleController extends AbstractActionController
{
    private $vehicleTable;

    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function indexAction()
    {
        // grab the paginator from the VehicleTable
        $paginator = $this->getVehicleTable()->fetchAll(true);

        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page
        $paginator->setItemCountPerPage(15);

        return new ViewModel(array(
            'paginator' => $paginator,
        ));
    }

    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function detailsAction()
    {
        $id = $this->params()->fromRoute('id');
        $vehicle = $this->getVehicleTable()->fetchAll(false, $id);

        return new ViewModel(array(
            'id'    => $id,
            'vehicle' => $vehicle->current(),
        ));
    }

    /**
    *
    * Action that retrieves all models
    * based on a maker - Ajax call
    *
    * @return ViewModel
    *
    */
    public function ajaxVehicleModelIdAction()
    {
        $request = $this->getRequest();
        $id = $request->getPost('vehicle_id');
        $model = $this->getVehicleTable()->getVehicleModelId($id);

        $viewModel = new ViewModel();
        $viewModel->setTemplate('');
        $viewModel->setTerminal($request->isXmlHttpRequest());

        return $viewModel->setVariables(array(
            'ajax' => Json::encode($model, true)
        ));
    }

    /**
    *
    * Action that renders the add view
    * to create new vehicles
    *
    * @return array
    *
    */
    public function addAction()
    {
        $sm = $this->getServiceLocator();
        $form = new VehicleForm($sm->get('MakerTableGateway'), 'add_model_form');
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $vehicle = new Vehicle();
            $form->setInputFilter($vehicle->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $vehicle->exchangeArray($form->getData());
                $this->getVehicleTable()->saveVehicle($vehicle);

                // Redirect to list of vehicles
                return $this->redirect()->toRoute('zfcadmin/vehicles');
            }
        }
        return array('form' => $form);
    }

    /**
    *
    * Action that renders the edit view
    * to edit vehicles
    *
    * @return array
    *
    */
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/vehicles', array(
                'action' => 'add'
            ));
        }

        // Get the Vehicle with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $vehicle = $this->getVehicleTable()->getVehicle($id);
            $vehicle->price = number_format($vehicle->price, 2, '.', '');
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('zfcadmin/vehicles', array(
                'action' => 'index'
            ));
        }

        $sm = $this->getServiceLocator();
        $form = new VehicleForm($sm->get('MakerTableGateway'), 'edit_vehicle_form');
        $form->bind($vehicle);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($vehicle->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getVehicleTable()->saveVehicle($vehicle);

                // Redirect to list of vehicles
                return $this->redirect()->toRoute('zfcadmin/vehicles');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
    *
    * Action that renders the delete view
    * to remove vehicles
    *
    * @return array
    *
    */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/vehicles');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getVehicleTable()->deleteVehicle($id);
            }

            // Redirect to list of vehicles
            return $this->redirect()->toRoute('zfcadmin/vehicles');
        }

        return array(
            'id'    => $id,
            'vehicle' => $this->getVehicleTable()->getVehicle($id)
        );
    }

    /**
    *
    * Method to get the vehicleTable object
    * if it's not set yet, the service locator
    * is called and retrieves an instance of it
    *
    * @return VehicleTable
    *
    */
    public function getVehicleTable()
    {
        if (!$this->vehicleTable) {
            $sm = $this->getServiceLocator();
            $this->vehicleTable = $sm->get('Vehicle\Model\VehicleTable');
        }
        return $this->vehicleTable;
    }
}
