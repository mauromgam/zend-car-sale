<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Vehicle;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Vehicle\Model\VehicleTable;
use Vehicle\Model\Vehicle;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Elasticsearch\Client;


class Module implements AutoloaderProviderInterface, ConfigProviderInterface
 {
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Vehicle\Model\VehicleTable' =>  function($sm) {
                    $tableGateway = $sm->get('VehicleTableGateway');
                    $params['hosts'] = array(
                        '127.0.0.1:9200'
                    );
                    $elasticSearchClient = new Client($params);
                    $table = new VehicleTable($tableGateway, $elasticSearchClient);
                    return $table;
                },
                'VehicleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Vehicle());
                    return new TableGateway('vehicle', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
 }