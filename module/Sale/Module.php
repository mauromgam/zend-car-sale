<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Sale;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Sale\Model\SaleTable;
use Sale\Model\Sale;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;


class Module implements AutoloaderProviderInterface, ConfigProviderInterface
 {
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Sale\Model\SaleTable' =>  function($sm) {
                    $tableGateway = $sm->get('SaleTableGateway');
                    $cartTable = $sm->get('Cart\Model\CartTable');
                    $table = new SaleTable($tableGateway, $cartTable);
                    return $table;
                },
                'SaleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Sale());
                    return new TableGateway('sale', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
 }