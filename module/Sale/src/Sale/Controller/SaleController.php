<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Sale\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Sale\Model\Sale;

class SaleController extends AbstractActionController
{

    protected $saleTable;


    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function indexAction()
    {
        $sm = $this->getServiceLocator();
        $cart = $sm->get('zfcuser_auth_service');
        $zfcUserAuth = $this->getServiceLocator()->get('zfcuser_auth_service');

        if (!$zfcUserAuth->getIdentity()) {
            return $this->redirect()->toRoute('home');
        }

        $sales = $this->getSaleTable()->getSales($zfcUserAuth->getIdentity()->getId());
        $data = array();
        foreach ($sales as $sale) {
            $sale->details = \Zend\Json\Json::decode($sale->details);
            $data[] = $sale;
        }

        return new ViewModel(array(
            'sales'       => $data,
            'sales_count' => $sales->getDataSource()->count(),
            'total'       => 0
        ));
    }

    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function buyAction()
    {
        $cartTable = $this->getServiceLocator()->get('Cart\Model\CartTable');
        $saleTable = $this->getServiceLocator()->get('Sale\Model\SaleTable');

        $zfcUserAuth = $this->getServiceLocator()->get('zfcuser_auth_service');

        $products = $cartTable->getExistingProductCart(0, $zfcUserAuth->getIdentity()->getId());

        $sale = new Sale();
        foreach ($products as $key => $product) {
            $sale->exchangeArray($product);
            $saleTable->saveSale($sale);
        }

        $cartTable->deleteCartFromUser($zfcUserAuth->getIdentity()->getId());


        return $this->redirect()->toRoute('sale');
    }

    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function deleteFromCartAction()
    {

        $cartTable = $this->getServiceLocator()->get('Cart\Model\CartTable');
        $vehicleId = $this->params()->fromPost('id');
        $token     = $this->params()->fromPost('token');

        $zfcUserAuth = $this->getServiceLocator()->get('zfcuser_auth_service');
        $cartTable->removeFromCart($vehicleId, $zfcUserAuth->getIdentity()->getId(), $token);

        return $this->redirect()->toRoute('cart');
    }

        /**
    *
    * Method to get the saleTable object
    * if it's not set yet, the service locator
    * is called and retrieves an instance of it
    *
    * @return SaleTable
    *
    */
    public function getSaleTable()
    {
        if (!$this->saleTable) {
            $sm = $this->getServiceLocator();
            $this->saleTable = $sm->get('Sale\Model\SaleTable');
        }
        return $this->saleTable;
    }
}
