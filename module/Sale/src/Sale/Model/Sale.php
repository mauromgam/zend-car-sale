<?php
namespace Sale\Model;

use Zend\Json\Json;

/**
 * Description of Sale
 *
 * @author mauro_000
 */
class Sale
{

    public $id;
    public $user_id;
    public $product_id;
    public $product_name;
    public $quantity;
    public $price_unit;
    public $details;

    public function exchangeArray($data)
    {
        $details = null;
        if (isset($data['options'])) {
            $details = $data['options'];
        } elseif (isset($data['details'])) {
            $details = $data['details'];
        }

        $this->id           = (!empty($data['id'])) ? $data['id'] : null;
        $this->user_id      = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->product_id   = (!empty($data['product_id'])) ? $data['product_id'] : null;
        $this->product_name = (!empty($data['product_name'])) ? $data['product_name'] : null;
        $this->quantity     = (!empty($data['quantity'])) ? $data['quantity'] : null;
        $this->price_unit   = (!empty($data['price_unit'])) ? $data['price_unit'] : null;
        $this->details      = $details;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
