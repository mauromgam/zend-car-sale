<?php
namespace Sale\Model;

use Zend\Db\TableGateway\TableGateway;
use Cart\Model\CartTable;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Sql;

class SaleTable
{
    protected $tableGateway;
    protected $cartTable;

    public function __construct(TableGateway $tableGateway, CartTable $cartTable)
    {
        $this->tableGateway = $tableGateway;
        $this->cartTable = $cartTable;
    }

    public function getSales($user_id, $limit=0)
    {
        $user_id  = (int) $user_id;
        $select = new Select('sale');
        $where = new Where();
        $where->equalTo('user_id', $user_id);
        $select->where($where);
        if ($limit != 0) {
            $select->limit($limit);
        }

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $rowset = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

        return $rowset;
    }

    public function saveSale(Sale $sale)
    {
        $data = array(
            'user_id'      => $sale->user_id,
            'product_id'   => $sale->product_id,
            'product_name' => $sale->product_name,
            'quantity'     => $sale->quantity,
            'price_unit'   => $sale->price_unit,
            'details'      => $sale->details,
        );

        try {
            $this->tableGateway->insert($data);
        } catch (\Exception $e) {
            var_dump($e);
        }
    }
}