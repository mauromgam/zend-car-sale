<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cart\Model\Sale;

class CartController extends AbstractActionController
{
    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function indexAction()
    {
        $sm = $this->getServiceLocator();
        $cart = $sm->get('ZendCart');

        return new ViewModel(array(
            'items' => $cart->cart(),
            'total_items' => $cart->total_items(),
            'total' => $cart->total(),
            'userIdentity' => $sm->get('zfcuser_auth_service')->getIdentity()
        ));
    }

    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function addToCartAction()
    {
        $cartTable = $this->getServiceLocator()->get('Cart\Model\CartTable');
        $vehicleId = $this->params()->fromPost('id');
        $qty       = (int) $this->params()->fromPost('qty');
        $qty       = abs($qty == 0 ? 1 : $qty);
        $action    = $this->params()->fromPost('act');

        $zfcUserAuth = $this->getServiceLocator()->get('zfcuser_auth_service');
        $cartTable->addToCart($vehicleId, $zfcUserAuth->getIdentity(), $qty, $action);

        return $this->redirect()->toRoute('cart');
    }

    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function deleteFromCartAction()
    {

        $cartTable = $this->getServiceLocator()->get('Cart\Model\CartTable');
        $vehicleId = $this->params()->fromPost('id');
        $token     = $this->params()->fromPost('token');

        $zfcUserAuth = $this->getServiceLocator()->get('zfcuser_auth_service');
        if ($zfcUserAuth->getIdentity()) {
            $user_id = $zfcUserAuth->getIdentity()->getId();
        } else {
            $user_id = 0;
        }
        $cartTable->removeFromCart($vehicleId, $user_id, $token);

        return $this->redirect()->toRoute('cart');
    }
}
