<?php
namespace Cart\Model;

use Zend\Db\TableGateway\TableGateway;
use Vehicle\Model\VehicleTable;
use ZendCart\Controller\Plugin\ZendCart;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Json\Json;

class CartTable
{

    protected $tableGateway;
    protected $vehicleTable;
    protected $cart;

    const MINUS_SYMBOL = '-';
    const PLUS_SYMBOL  = '+';


    public function __construct(TableGateway $tableGateway, VehicleTable $vehicleTable, ZendCart $cart)
    {
        $this->tableGateway = $tableGateway;
        $this->cart = $cart;
        $this->vehicleTable = $vehicleTable;
    }

    public function getCart($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function checkOnlyZendCartHasValues($userIdentity)
    {
        $return = $this->getExistingProductCart(0,$userIdentity->getId());
        return $this->cart->cart() && $return->getDataSource()->count() == 0;
    }

    public function checkOnlyDatabaseHasValues($userIdentity)
    {
        $return = $this->getExistingProductCart(0,$userIdentity->getId());
        return !$this->cart->cart() && $return->getDataSource()->count() > 0;
    }

    /**
     * Method called always that the system has an inconsistency
     * i.e., when there is data in the ZendCart but not in the
     * Database and vice-versa
     */
    public function initializeCart($userIdentity)
    {

        // #1 - There are products in the ZendCart but not in Database
        if ($this->checkOnlyZendCartHasValues($userIdentity)) {

            // need to insert values from ZendCart to Database
            $this->transferZendCartToDatabase($userIdentity->getId());

        }

        // #2 - There are products in the Database but not in ZenCart
        if ($this->checkOnlyDatabaseHasValues($userIdentity)) {

            // need to insert values from Database to ZendCart
            $this->transferDatabaseToZendCart($userIdentity->getId());

        }

    }

    public function transferZendCartToDatabase($user_id)
    {

        // get all products added to ZendCart
        $zendCartData = $this->cart->cart();

        $product = array();
        $cart = new Cart();
        foreach ($zendCartData as $item) {
            $product = $this->setProductFromZCtoDBArray($item, $user_id);

            $cart->exchangeArray($product);
            $this->saveCart($cart);
        }

    }

    public function transferDatabaseToZendCart($user_id)
    {

        // get all products inserted into Database
        $databaseData = $this->getExistingProductCart(0,$user_id);

        $product = array();
        $cart = new Cart();
        foreach ($databaseData as $item) {
            $product = $this->setProductFromDBtoZCArray($item, $user_id);
            $this->cart->insert($product);
        }

    }

    public function setProductFromZCtoDBArray($item, $user_id)
    {

        $data = array(
            'id'           => null, // That is because the data doesn't exist in the Database
            'user_id'      => $user_id,
            'product_id'   => $item['id'],
            'product_name' => $item['name'],
            'quantity'     => $item['qty'],
            'price_unit'   => $item['price'],
            'options'      => $item['options'],
        );

        return $data;

    }

    public function setProductFromDBtoZCArray($item, $user_id)
    {

        $data = array(
            'id'          => $item['product_id'],
            'product_id'  => $item['product_id'],
            'qty'         => $item['quantity'],
            'price'       => $item['price_unit'],
            'name'        => $item['product_name'],
            'options'     => Json::decode($item['options']),
        );

        return $data;

    }

    public function setProductFromDBtoCartArray($item)
    {

        $data = array(
            'id'           => $item['id'],
            'user_id'      => $item['user_id'],
            'product_id'   => $item['product_id'],
            'product_name' => $item['product_name'],
            'quantity'     => $item['quantity'],
            'price_unit'   => $item['price_unit'],
            'options'      => Json::decode($item['options']),
        );

        return $data;

    }

    public function setProductFromDBtoCartArray2($item, $user_id, $qty)
    {
        $item['options'] = array(
            'id'           => $item['id'],
            'year'         => $item['year'],
            'registration' => $item['registration'],
            'engine_size'  => $item['engine_size'],
            'keyword'      => $item['keyword'],
            'maker_id'     => $item['maker_id'],
            'model_id'     => $item['model_id'],
        );
        $name = $item['year'] . ' ' .
            $item['maker_id'] . ' ' .
            $item['model_id'] . ' ' .
            $item['engine_size'];
        $data = array(
            'product_id'   => $item['id'],
            'user_id'      => $user_id,
            'product_name' => $name,
            'quantity'     => ($qty ? $qty : 1),
            'price_unit'   => $item['price'],
            'options'      => $item['options'],
        );

        return $data;

    }

    public function setProductFromDBtoZCArray2($item, $qty)
    {

        $item['options'] = array(
            'id'           => $item['id'],
            'year'         => $item['year'],
            'registration' => $item['registration'],
            'engine_size'  => $item['engine_size'],
            'keyword'      => $item['keyword'],
            'maker_id'     => $item['maker_id'],
            'model_id'     => $item['model_id'],
        );
        $name = $item['year'] . ' ' .
            $item['maker_id'] . ' ' .
            $item['model_id'] . ' ' .
            $item['engine_size'];
        $data = array(
            'id'          => $item['id'],
            'product_id'  => $item['id'],
            'qty'         => ($qty ? $qty : 1),
            'price'       => $item['price'],
            'name'        => $name,
            'options'     => $item['options'],
        );

        return $data;

    }

    public function getExistingProductCart($product_id, $user_id)
    {

        // create a new Select object for the table vehicle
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select('cart');
        $where = new Where();
        if ($product_id != 0) {
            $where->equalTo('product_id', $product_id);
        }
        $where->equalTo('user_id', $user_id);
        $select->where($where);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

        return $result;

    }

    public function checkZendCartHasValues()
    {
        return !empty($this->cart->cart());
    }

    /**
     * Method called when user wants to add a vehicle to the cart/basket.
     * It can increase or decrease the number of stored vehicles.
     */
    public function addToCart($vehicle_id, $userIdentity, $qty=0, $action=self::PLUS_SYMBOL)
    {
        // Check if the user is increasing or decreasing
        // the number of products
        if ($action == self::MINUS_SYMBOL) {
            $qty = $qty * (-1);
        }

        // Lets separete the insertion/update
        // It must be done in both ends, ZendCart and Database
        if ($this->checkZendCartHasValues()) {

            $this->updateZendCart($vehicle_id, $qty, $userIdentity);

        } else {

            $this->insertZendCart($vehicle_id, $qty);

        }

        // User must be logged in to insert cart data into Database
        if ($userIdentity) {
            // Database uses the same method to insert and update its values
            $this->insertDatabase($vehicle_id, $userIdentity, $qty);
        }
    }

    public function insertDatabase($vehicle_id, $userIdentity, $qty)
    {
        $cart = new Cart();
        $user_id = $userIdentity ? $userIdentity->getId() : 0;
        $zendCartData = $this->cart->cart();

        // #1 Check if this user has already added this vehicle to his cart
        $existingDatabaseData = $this->getExistingProductCart($vehicle_id, $user_id);
        if ($existingDatabaseData->getDataSource()->count()) {

            $data = $existingDatabaseData->current();
            $product = $this->setProductFromDBtoCartArray($data);

            // If qty is smaller or igual to zero
            // remove vehicle from both ZendCart and
            // Database and then return
            $product['quantity'] = $product['quantity'] + $qty;
            $deleted = $this->checkIfRemove($product['quantity'], $zendCartData, $vehicle_id, $user_id);

            if (!$deleted) {
                $cart->exchangeArray($product);
                $this->saveCart($cart);
            }

        } elseif ($qty > 0) {

            // #2 There is no data of this vehicle in this user's cart
            $databaseData = $this->vehicleTable->fetchAll(false,$vehicle_id)->current();

            $product = $this->setProductFromDBtoCartArray2($databaseData, $userIdentity->getId(), $qty);
            unset($product['id']);
            $cart->exchangeArray($product);
            $this->saveCart($cart);

        }

    }

    public function insertZendCart($vehicle_id, $qty)
    {

        // Get vehicle information based on $vehicle_id
        $databaseData = $this->vehicleTable->fetchAll(false,$vehicle_id)->current();

        $product = $this->setProductFromDBtoZCArray2($databaseData, $qty);

        $this->cart->insert($product);

    }


    public function updateZendCart($vehicle_id, $qty, $userIdentity)
    {
        $token = '';
        $current_qty = 0;
        $zendCartData = $this->cart->cart();
        $user_id = $userIdentity ? $userIdentity->getId() : 0;

        // Search for the token of the product
        // that need to be updated
        foreach ($zendCartData as $key => $value) {
            if ($value['id'] == $vehicle_id) {
                $token = $key;
                $current_qty = $value['qty'];
                break;
            }
        }

        // If qty is smaller or igual to zero
        // remove vehicle from both ZendCart and
        // Database and then return
        $qty = $current_qty + $qty;
        if ($this->checkIfRemove($qty, $zendCartData, $vehicle_id, $user_id)) {
            return;
        }

        if ($token != '' && $current_qty != 0) {
            $product = array(
                'qty'   => $qty,
                'token' => $token
            );
            $this->cart->update($product);
        } else {
            $this->insertZendCart($vehicle_id, $qty);
        }

    }

    public function saveCart(Cart $cart)
    {
        $data = array(
            'user_id'      => $cart->user_id,
            'product_id'   => $cart->product_id,
            'product_name' => $cart->product_name,
            'quantity'     => $cart->quantity,
            'price_unit'   => $cart->price_unit,
            'options'      => $cart->options,
        );

        $id = (int) $cart->id;

        if ($id == 0) {
            try {
                $this->tableGateway->insert($data);
            } catch (\Exception $e) {
                var_dump($e);
            }
        } else {
            if (!empty($this->getExistingProductCart($data['product_id'], $data['user_id'])->current())) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Vehicle id does not exist');
            }
        }
    }

    public function checkIfRemove($qty, $zendCart, $product_id, $user_id=0)
    {

        if ($qty <= 0 && is_array($zendCart)) {
            $token = '';
            foreach ($zendCart as $key => $item) {
                if ($item['id'] == $product_id) {
                    $token = $key;
                    break;
                }
            }
            $this->removeFromCart($product_id, $user_id, $token);
            return true;
        }
        return false;
    }

    public function removeFromCart($vehicle_id, $user_id, $token)
    {

        if ($token != '') {
            $this->cart->remove(array(
                'token' => $token
            ));
        }

        // Can only remove from Database if user_id is not zero,
        // otherwise could remove accidentaly a
        // vehicle from other user that added the same one
        // to his cart
        if ($user_id != 0) {
            $this->tableGateway->delete(array(
                'product_id' => $vehicle_id,
                'user_id'    => $user_id
            ));
        }
    }

    public function deleteCartFromUser($user_id)
    {
        $this->cart->destroy();
        $this->tableGateway->delete(array(
            'user_id' => (int) $user_id
        ));
    }

    public function setProductArray($vehicle, $qty=0)
    {
        // it can be either an array from database as well as from ZendCart
        $year         = isset($vehicle['year']) ? $vehicle['year'] : $vehicle['options']['year'];
        $registration = isset($vehicle['registration']) ? $vehicle['registration'] : $vehicle['options']['registration'];
        $engine_size  = isset($vehicle['engine_size']) ? $vehicle['engine_size'] : $vehicle['options']['engine_size'];
        $keyword      = isset($vehicle['keyword']) ? $vehicle['keyword'] : $vehicle['options']['keyword'];
        $maker_id     = isset($vehicle['maker_id']) ? $vehicle['maker_id'] : $vehicle['options']['maker_id'];
        $model_id     = isset($vehicle['model_id']) ? $vehicle['model_id'] : $vehicle['options']['model_id'];

        $product = array(
            'id'          => $vehicle['id'],
            'product_id'  => $vehicle['id'],
            'qty'         => ($qty ? $qty : 1),
            'price'       => $vehicle['price'],
            'name'        => trim(
                $year . ' ' .
                $maker_id . ' ' .
                $model_id . ' ' .
                $engine_size
            ),
            'options' => array(
                'id'           => $vehicle['id'],
                'year'         => $year,
                'registration' => $registration,
                'engine_size'  => $engine_size,
                'keyword'      => $keyword,
                'maker_id'     => $maker_id,
                'model_id'     => $model_id,
            )
        );

        return $product;
    }
}