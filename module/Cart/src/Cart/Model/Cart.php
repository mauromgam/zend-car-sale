<?php
namespace Cart\Model;

use Zend\Json\Json;

/**
 * Description of Cart
 *
 * @author mauro_000
 */
class Cart
{

    public $id;
    public $user_id;
    public $product_id;
    public $product_name;
    public $quantity;
    public $price_unit;
    public $options;

    public function exchangeArray($data)
    {
        $this->id           = (!empty($data['id'])) ? $data['id'] : null;
        $this->user_id      = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->product_id   = (!empty($data['product_id'])) ? $data['product_id'] : null;
        $this->product_name = (!empty($data['product_name'])) ? $data['product_name'] : null;
        $this->quantity     = (!empty($data['quantity'])) ? $data['quantity'] : null;
        $this->price_unit   = (!empty($data['price_unit'])) ? $data['price_unit'] : null;
        $this->options      = (!empty($data['options'])) ? Json::encode($data['options']) : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
