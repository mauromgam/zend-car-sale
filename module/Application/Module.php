<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $sm = $e->getApplication()->getServiceManager();
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $utilsService = $sm->get('Application\Model\Utils');

        $viewModel->query = $utilsService->getQuerySearchParamValue();

        $zfcUserAuth = $sm->get('zfcuser_auth_service');
        $viewModel->userIdentity = $zfcUserAuth->getIdentity();
        if ($zfcUserAuth->getIdentity()) {
            $userIdentity = $zfcUserAuth->getIdentity();
            $viewModel->userDisplayName = $userIdentity->getDisplayName();
            $viewModel->userRole = $sm->get('User\User\UserTable')
                ->getUserRole($userIdentity->getId(), $sm->get('Zend\Db\Adapter\Adapter'));

            $sale = $sm->get('Sale\Model\SaleTable');
            $sales = $sale->getSales($userIdentity->getId(), 1);
            $viewModel->salesHistory = $sales->getDataSource()->count();
        }

        $cart = $sm->get('ZendCart');
        if ($cart->cart()) {
            $viewModel->cart       = true;
            $viewModel->totalItems = $cart->total_items();
        } else {
            $viewModel->cart = false;
        }

        $this->initAcl($e);
        $e->getApplication()
            ->getEventManager()
            ->attach('route', array($this, 'checkAcl'));
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getDbRoles(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $results = $sm->get('Application\Model\UserRoleTable')->getRoles();

        // making the roles array
        $roles = array();
        foreach($results as $result){
            $roles[$result['role']][] = [
                'resource' => $result['resource'],
                'permission' => $result['permission'],

            ];
        }
        return $roles;
    }

    public function initAcl(MvcEvent $e)
    {

        $acl = new \Zend\Permissions\Acl\Acl();
        $roles = $this->getDbRoles($e);

        foreach ($roles as $role => $resources) {

            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);
            $acl->addRole($role);

            //adding resources
            foreach ($resources as $resource) {
                if(!$acl->hasResource($resource['permission'])) {
                   $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource['permission']));
                }
            }

            //adding restrictions
            foreach ($resources as $resource) {
                $acl->allow($role, $resource['permission']);
            }
        }

        //setting to view
        $e->getViewModel()->acl = $acl;

    }

    public function checkAcl(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $zfcUserAuth = $sm->get('zfcuser_auth_service');
        if ($zfcUserAuth->getIdentity() != null) {
            $userRole = $sm->get('User\User\UserTable')
                ->getUserRole($zfcUserAuth->getIdentity()->getId(), $sm->get('Zend\Db\Adapter\Adapter'));
        } else {
            $userRole = 'guest';
        }

        $route = $e->getRouteMatch()->getMatchedRouteName();
        //var_dump($e->getViewModel()->acl->isAllowed($userRole, $route));
        //var_dump($e->getViewModel()->acl->hasResource($route));
        //var_dump($route);die;
        if (!$e->getViewModel()->acl->hasResource($route) || !$e->getViewModel()->acl->isAllowed($userRole, $route)) {
            $response = $e->getResponse();
            //location to page or what ever
            $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
            $response->setStatusCode(404);

        }
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\UserRoleTable' =>  function($sm) {
                    $tableGateway = $sm->get('UserRoleTableGateway');
                    $table = new Model\UserRoleTable($tableGateway);
                    return $table;
                },
                'UserRoleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\UserRole());
                    return new \Zend\Db\TableGateway\TableGateway('user_role', $dbAdapter, null, $resultSetPrototype);
                },
                'ZendCart' => function ($sm) {
                    $cartConfig = array(
                        'on_insert_update_existing_item' => true,
                        'vat'  => 1
                    );
                    $zendCart = new \ZendCart\Controller\Plugin\ZendCart($cartConfig);
                    return new $zendCart;
                },
                'Application\Model\Utils' => function ($sm) {
                    return new Model\Utils();
                },
            ),
        );
    }
}
