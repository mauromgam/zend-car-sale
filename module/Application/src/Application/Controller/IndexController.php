<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Vehicle\Model\VehicleElasticSearch;

class IndexController extends AbstractActionController
{
    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view
    *
    * @return ViewModel
    *
    */
    public function indexAction()
    {
        $sm = $this->getServiceLocator();
        $zfcUserAuth = $sm->get('zfcuser_auth_service');

        // intialize cart
        if ($zfcUserAuth->getIdentity()) {
            $cart = $this->getServiceLocator()->get('ZendCart');
            $cart_items = $sm->get('Cart\Model\CartTable')->getExistingProductCart(0, $zfcUserAuth->getIdentity()->getId());
            if ((!$cart->cart() &&
                $cart_items->getDataSource()->count() > 0) ||
                ($cart->cart() &&
                $cart_items->getDataSource()->count() == 0)
            ) {
                $sm->get('Cart\Model\CartTable')->initializeCart($zfcUserAuth->getIdentity());
                return $this->redirect()->toRoute('home');
            }
        }

        $request = $this->getRequest();
        $q = $request->getQuery('q');
        if ($q != null) {
            $q = urlencode($q);
            $q = "q=$q";
        }
        $vehicles = VehicleElasticSearch::getElasticSearchResults($q);

        return new ViewModel(array(
            'vehicles' => $vehicles,
            'q'        => $request->getQuery('q')
        ));
    }
}
