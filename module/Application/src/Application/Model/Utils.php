<?php
namespace Application\Model;

use Zend\Http\PhpEnvironment\Request;

class Utils
{
    const DECIMAL_SEPARATOR = '.';
    const THOUSAND_SEPARATOR = ',';
    
    /**
    *
    * Action that retrieves all vehicles
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function getQuerySearchParamValue() {
        $request = new Request();
        $q = $request->getQuery('q');

        return $q;
    }

}
