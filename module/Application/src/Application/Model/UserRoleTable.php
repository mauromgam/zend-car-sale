<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class UserRoleTable
{
    protected $table;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->table = 'user_role';
        $this->tableGateway = $tableGateway;
    }

    public function getRoles() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select(array('r' => 'role'));
        $select->join(array('rp' => 'role_permission'),
                'r.rid = rp.role_id')
            ->join(array('p' => 'permission'),
                'rp.permission_id = p.id',
                array('permission' => 'permission_name'))
            ->join(array('rs' => 'resource'),
                'p.resource_id = rs.id',
                array('resource' => 'resource_name'));
        $select->columns(array(
            'role' => 'role_name',
        ));
        $select->order(array(
            'role_name',
            'permission_name',
            'resource_name',
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

        public function saveUserRole(UserRole $user_role)
    {
        $data = array(
            'user_id' => $user_role->user_id,
            'role_id' => $user_role->role_id,
        );

        $id = (int) $user_role->id;
        if ($id == 0) {
            try {
               $this->tableGateway->insert($data);
            } catch (\Exception $e) {
                var_dump($e);
            }
        } else {
            if ($this->getVehicle($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User role id does not exist');
            }
        }
    }

    public function deleteUserRole($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}