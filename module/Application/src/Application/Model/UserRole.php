<?php
namespace Application\Model;

class UserRole
{
    public $id;
    public $user_id;
    public $role_id;

    public function exchangeArray($data)
    {
        $this->id      = (!empty($data['id'])) ? $data['id'] : null;
        $this->user_id = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->role_id = (!empty($data['role_id'])) ? $data['role_id'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
