<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Paginator;

class UserTable
{
    protected $table;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->table = 'user';
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if ($paginated) {
            // create a new Select object for the table user
            $select = new Select($this->table);
            // create a new result set based on the User entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new User());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);

            return $paginator;
        }
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getUser($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('user_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception('Could not find row $id');
        }

        return $row;
    }

    /**
     * Method accessed from Application\Module.php
     * Must retrieve user role
     */
    public static function getUserRole($id, $adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select('user_role');
        $select->join('role',
                'user_role.role_id = role.rid',
                array('role_name'));
        $select->where(array('user_role.user_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current();

        return $result['role_name'];
    }

    public function saveUser(User $user)
    {
        $data = array(
            'username'     => $user->username,
            'email'        => $user->email,
            'display_name' => $user->display_name,
            'password'     => $user->password,
            'state'        => $user->state,
        );

        // need to encrypt user password
        // use the same encryption of ZFCUser

        $id = (int) $user->user_id;
        if ($id == 0) {
            try {
               $this->tableGateway->insert($data);
            } catch (\Exception $e) {
                var_dump($e);
            }
        } else {
            if ($this->getUser($id)) {
                $this->tableGateway->update($data, array('user_id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(array('user_id' => (int) $id));
    }
}