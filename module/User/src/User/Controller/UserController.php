<?php
/**
 * UserController
 *
 * @author Mauro Marinho
 */
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Model\User;
use User\Form\UserForm;

class UserController extends AbstractActionController
{
    private $userTable;

    /**
    *
    * Action that retrieves all users
    * and prints it in the view, paginated
    *
    * @return ViewUser
    *
    */
    public function indexAction()
    {
        // grab the paginator from the UserTable
        $paginator = $this->getUserTable()->fetchAll(true);

        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page
        $paginator->setItemCountPerPage(15);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }

    /**
    *
    * Action that renders the add view
    * to create new users
    *
    * @return array
    *
    */
    public function addAction()
    {
        $sm = $this->getServiceLocator();
        $form = new UserForm('add_user_form');
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new User();
            $form->setInputFilter($user->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $user->exchangeArray($form->getData());
                $this->getUserTable()->saveUser($user);

                // Redirect to list of users
                return $this->redirect()->toRoute('zfcadmin/users');
            }
        }
        return array('form' => $form);
    }

    /**
    *
    * Action that renders the edit view
    * to edit users
    *
    * @return array
    *
    */
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('user_id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/users', array(
                'action' => 'add'
            ));
        }

        // Get the User with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $user = $this->getUserTable()->getUser($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('zfcadmin/users', array(
                'action' => 'index'
            ));
        }

        $form = new UserForm('edit_user_form');
        $form->bind($user);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($user->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getUserTable()->saveUser($user);

                // Redirect to list of users
                return $this->redirect()->toRoute('zfcadmin/users');
            }
        }

        return array(
            'user_id' => $id,
            'form' => $form,
        );
    }

    /**
    *
    * Action that renders the delete view
    * to remove users
    *
    * @return array
    *
    */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('user_id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/users');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('user_id');
                $this->getUserTable()->deleteUser($id);
            }

            // Redirect to list of users
            return $this->redirect()->toRoute('zfcadmin/users');
        }

        return array(
            'user_id'    => $id,
            'user' => $this->getUserTable()->getUser($id)
        );
    }

    /**
    *
    * Method to get the userTable object
    * if it's not set yet, the service locator
    * is called and retrieves an instance of it
    *
    * @return UserTable
    *
    */
    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\User\UserTable');
        }
        return $this->userTable;
    }
}
