<?php
namespace Maker\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class MakerTable
{
    protected $table;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->table = 'vehicle_maker';
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated=false)
    {
        if ($paginated) {
            // create a new Select object for the table maker
            $select = new Select($this->table);
            // create a new result set based on the Maker entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Maker());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select($this->table);

        return $resultSet;
    }

    public function getMaker($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function saveMaker(Maker $maker)
    {
        $data = array(
            'name'      => $maker->name,
        );

        $id = (int) $maker->id;
        if ($id == 0) {
            try {
               $this->tableGateway->insert($data);
            } catch (\Exception $e) {
                var_dump($e);
            }
        } else {
            if ($this->getMaker($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Maker id does not exist');
            }
        }
    }

    public function deleteMaker($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}