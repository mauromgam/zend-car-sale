<?php
/**
 * MakerController
 *
 * @author Mauro Marinho
 */
namespace Maker\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Maker\Model\Maker;
use Maker\Form\MakerForm;

class MakerController extends AbstractActionController
{
    private $makerTable;

    /**
    *
    * Action that retrieves all makers
    * and prints it in the view, paginated
    *
    * @return ViewModel
    *
    */
    public function indexAction()
    {
        // grab the paginator from the MakerTable
        $paginator = $this->getMakerTable()->fetchAll(true);

        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page
        $paginator->setItemCountPerPage(15);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }

    /**
    *
    * Action that renders the add view
    * to create new makers
    *
    * @return array
    *
    */
    public function addAction()
    {
        $form = new MakerForm('add_maker_form');
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $maker = new Maker();
            $form->setInputFilter($maker->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $maker->exchangeArray($form->getData());
                $this->getMakerTable()->saveMaker($maker);

                // Redirect to list of makers
                return $this->redirect()->toRoute('zfcadmin/makers');
            }
        }
        return array('form' => $form);
    }

    /**
    *
    * Action that renders the edit view
    * to edit makers
    *
    * @return array
    *
    */
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/makers', array(
                'action' => 'add'
            ));
        }

        // Get the Maker with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $maker = $this->getMakerTable()->getMaker($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('zfcadmin/makers', array(
                'action' => 'index'
            ));
        }

        $form  = new MakerForm('edit_maker_form');
        $form->bind($maker);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($maker->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getMakerTable()->saveMaker($maker);

                // Redirect to list of makers
                return $this->redirect()->toRoute('zfcadmin/makers');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
    *
    * Action that renders the delete view
    * to remove makers
    *
    * @return array
    *
    */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('zfcadmin/makers');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getMakerTable()->deleteMaker($id);
            }

            // Redirect to list of makers
            return $this->redirect()->toRoute('zfcadmin/makers');
        }

        return array(
            'id'    => $id,
            'maker' => $this->getMakerTable()->getMaker($id)
        );
    }

    /**
    *
    * Method to get the makerTable object
    * if it's not set yet, the service locator
    * is called and retrieves an instance of it
    *
    * @return MakerTable
    *
    */
    public function getMakerTable()
    {
        if (!$this->makerTable) {
            $sm = $this->getServiceLocator();
            $this->makerTable = $sm->get('Maker\Model\MakerTable');
        }
        return $this->makerTable;
    }
}
