Car Sales
=======================

Introduction
------------
This system comprehends some parts of an e-commerce web site.
It is a car sale e-commerce with initial functionalities such as user registration, list of available cars for sale,
search using ElasticSearch, basket/cart, lost cart (i.e., when a user is logged and add some products - vehicles - to his
basket and after this leave the web site. Those vehicles he added to his basket are saved to a later login, so he won't
need to search for them again.


The security access to the pages was implemented using ZF2\Acl. Users roles are guest, customer and admin, following to the
access rules I created.


Direct access to the application: http://178.62.14.112/


Instructions
------------
The ElasticSearch index 'vehicles' below is created when a vehicle is added, updated or deleted.
This action is executed only once by calling Vehicle\Model\VehicleElasticSearch::checkIndexExist();
curl -XPUT 'http://localhost:9200/vehicles/' -d '{"index":{"name":"vehicles","number_of_shards":1,"analysis":{"analyzer":{"translation_index_analyzer":{"type":"custom","tokenizer":"standard","filter":"standard,lowercase,translation_tokenizer"},"translation_search_analyzer":{"type":"custom","tokenizer":"standard","filter":"standard,lowercase"}},"filter":{"translation_tokenizer":{"type":"nGram","min_gram":"1","max_gram":"10"}}}}}'

After you download the files from the repository, you'll need to login in with an Admin user, then you go to the list of
vehicles in the Admin page and 'refresh' the vehicles indices, just by clicking in Edit and then Save.
As there is already data in the database, the ElasticSearch indices weren't created, they should be when the user creates
or edits a vehicle. After doing this, you'll be able to see the vehicles in the Home Page.


Having done this, you can add vehicles to the cart, increase an decrease its amount, remove or by.
Being logged into the system or not.


The user already created test@test.com has Admin privilege, its password is "123123123".


If you are using Apache, this system is using the PHP extension intl, so in order to get it working properly, you need
to copy all files with name starting with icu* from PHP root folder to Apache 'bin' folder and then restart all Apache
services.


I wasn't able to use the ZendCart configuration file, even with it placed in config/autoload, it didn't work, so I had to
edit \Zendcart\Controller\Plugin\ZendCart.php as follow.

from
    $shouldUpdate = $this->_config['on_insert_update_existing_item'];
to
    $shouldUpdate = false;

And

from
    'vat'       => isset($items['vat']) ? $items['vat'] : $this->_config['vat']
to
    'vat'       => 21
Without it, ZendCart won't work.

The database structure with some already loaded data is under 'data' folder as db.sql.

Third Party modules
-------------------
I made use of some third party modules listed below:
ZfcAdmin
ZfcUser
ZendCart


