/*
 * Customized JS
 */
function getModelsByMaker(maker_id) {
    $.ajax({
        url: '/admin/model/ajax-models',
        data: {"maker_id":maker_id},
        type: "POST",
        dataType: "json",
        success:function(data) {
            var options = "";
            jQuery.each(data, function(i, val) {
                options += '<option value="'+val.id+'">'+val.name+'</option>';
            });
            $(".model-select").empty().append(options);

            var url_path = window.location.pathname.split("/").reverse();
            if (!isNaN(url_path[0])) {
                getVehicleModelId(url_path[0]);
            }
        },
        error: function(errorThrown) {

        }
    });
}

function getVehicleModelId(vehicle_id) {
    $.ajax({
        url: '/admin/vehicle/ajax-vehicle-model-id',
        data: {"vehicle_id":vehicle_id},
        type: "POST",
        dataType: "json",
        success:function(data) {
            $(".model-select").val(data[0].id);
        },
        error: function(errorThrown) {

        }
    });
}

$(document).ready(function(){

    $('.money').mask('000.000.000.000.000,00', {placeholder: "000.000.000,00", reverse: true});

    if (typeof $('.maker-select').val() != 'undefined' && $('.maker-select').val() != '') {
        getModelsByMaker($('.maker-select').val());
    }

    $('.maker-select').on('change', function() {
        var maker_id = $(this).val();
        getModelsByMaker(maker_id);
    });

});
